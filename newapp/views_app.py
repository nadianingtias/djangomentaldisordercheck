from django.shortcuts import render, get_object_or_404, redirect

# dot is the current direktori
from .forms import TweetForm
from .models import TweetText
from .models import Sentics
from .models import Senticnet
from .models import WordList_sentic
from .models import TrainData
from django.utils import timezone

import nltk
import spacy
import re

from spacy.tokens import Doc
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk.stem.wordnet import WordNetLemmatizer
from nltk import word_tokenize, pos_tag
from nltk.corpus import wordnet
from collections import defaultdict
from nltk.corpus import stopwords
from textblob import TextBlob


nlp = spacy.load('en_core_web_sm')
sent_analyzer = SentimentIntensityAnalyzer()
tag_map = defaultdict(lambda : wordnet.NOUN)
tag_map['J'] = wordnet.ADJ
tag_map['V'] = wordnet.VERB
tag_map['R'] = wordnet.ADV
wn_lemmater = WordNetLemmatizer()
stop_words =set(stopwords.words('english'))

def sentiment_scores(doc):
    return sent_analyzer.polarity_scores(doc.text)


Doc.set_extension("sentimenter", getter=sentiment_scores)

def getNumberRemoval(text):
    # PREPROCESS - NUMBER REMOVAL
    text = re.sub(r'\d+', '', text)
    # print("Number removal: {}".format(text))
    return text
def getMentionLinkHashtagRemoval(text):
    # PREPROCESS - MENTION REMOVAL , LINK, HASHTAG sign REMOVAL
    text = re.sub(r'@\w+ ?|http\S+|#', '', text)
    # print("Mention, Link, hashtag sign removal: {}".format(text))
    return text
def getNTConversion(text):
    # PREPROCESS - n't conversion
    # text  = re.sub('n''t+$', " not", text)
    text = re.sub("n't\s*|don$", " not ", text)
    # print(" n't conversion: {}".format(text))
    return text
def getFivePreprocess(text):
    text = getNumberRemoval(text)  # PREPROCESS - NUMBER REMOVAL
    # PREPROCESS - PUNCTUATION REMOVAL (have done at prev preprocess)
    # text = text.translate(string ("", ""), string.punctuation)
    text = getMentionLinkHashtagRemoval(text)  # PREPROCESS - MENTION REMOVAL , LINK, HASHTAG sign REMOVAL
    text = getNTConversion(text)  # PREPROCESS - n't conversion
    # PREPROCESS - OVERWRITE (data dari DB sudah recognize by wordnet and corrected by textblob)
    # text = ''.join(''.join(s)[:] for _, s in itertools.groupby(text))
    return text
#================KEBUTUHAN PREPROCESS REGEX
regex_str = [
    # r'<[^>]+>',  # HTML tags
    # r'(?:@[\w_]+)',  # @-mentions
    # r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)",  # hash-tags
    # r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+',  # URLs
    # r'(?:(?:\d+,?)+(?:\.?\d+)?)',  # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])",  # words with - and '
    r'(?:[\w_]+)',  # other words
    # r'(?:\S)'  # anything else
]
tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
def tokenize(s):
    return tokens_re.findall(s)
def preprocess(param, lowercase=False):
    tokens = tokenize(param)
    if lowercase:
        tokens = [token.lower() for token in tokens]
    return tokens

#===================KEBUTUHAN STOPWORD
self_words = ['my', 'myself', 'i', "i'", 'self', 'am', 'me', 'id', "i'd", "'d", "ain", "ain't",
                  "i'll", 'im', "i'm", "ive","i've",
                  "mine", "own", 'myselves', 'ourselves', "'ve"]
negation_words = ['no', 'not', 'mustn', "wouldn't", "aren't", "hasn't", 'wasn', 'don',
                      "isn't", 'won', "won't", "didn't", "couldn't", "weren't", 'nor', 'neither', "'t"]
def updateStopWords(stop_words):
    to_extend = ['x', 'y', 'r', 'e', 's', 'm', 'hi', 'yet', 'may', 'oh', 'due', 'to',
                 'day', 'days', 'weeks', 'week',
                 'being', 'months', 'way', ]
    stop_words = stop_words.union(to_extend)

    # print(stop_words)
    to_remove = ['instead']
    stop_words = stop_words.difference(to_remove)
    stop_words = stop_words.difference(self_words)
    stop_words = stop_words.difference(negation_words)
    # print(stop_words)
    return  stop_words

stop_words = updateStopWords(stop_words)

# 1st - SELF REFERENCES - check
def getSelfScore(text):
    selfScore = False
    # for word in filtered_sentence:
    #     if word in self_words:
    #     selfScore = True;
    #     break

# Create your views here.
# parameter bernama harus persis dengan yang di define di urls.py

def home(request, template_name = 'mdcek/index.html' ):
    # if request.method == 'POST':
        form = TweetForm(request.POST)
        if form.is_valid():

            post = form.save(commit=False)
            # post.author = request.user
            # post.published_date = timezone.now()
            post.selfScore = True
            post.save()
            return redirect('result', pk=post._id)
    # else:
        wordlist = WordList_sentic.objects.filter()
        # form = TweetForm()
        return render(request, template_name, {'formInputTweet' : form, 'wordlist':wordlist})

def index(request, template_name='mdcek/index.html'):
    #load wordlist saya disini
    wordlist = WordList_sentic.objects.filter()
    wordSenticList = set()
    for word in wordlist:
        wordSenticList.add(word.word)

    # load trainData disini
    traindata = TrainData.objects.filter()
    # hitung range existing di atribut MDscore
    arrayMDScoreSorted = []
    for data in traindata:
        if (data.MDscore != 0):
            arrayMDScoreSorted.append(data.MDscore)
    arrayMDScoreSorted.sort()
    print(" MDscore keseluruhan : {}".format(arrayMDScoreSorted))

    lenMDscore = len(arrayMDScoreSorted)
    print("len : {}".format(lenMDscore))
    print("Q1 len array of MD Score : {} ".format(arrayMDScoreSorted[int((lenMDscore + 1) * 1 / 4)]))
    print("Q2 len array of MD Score : {} ".format(arrayMDScoreSorted[int((lenMDscore + 1) / 2)]))
    print("Q3 len array of MD Score : {} ".format(arrayMDScoreSorted[int((lenMDscore + 1) * 3 / 4)]))
    q1 = arrayMDScoreSorted[int((lenMDscore + 1) * 1 / 4)]
    q2 = arrayMDScoreSorted[int((lenMDscore + 1) / 2)]
    q3 = arrayMDScoreSorted[int((lenMDscore + 1) * 3 / 4)]
    interquartile = q3 - q1
    batasOutlierBawah = q1 - (1.5 * interquartile)
    batasOutlierAtas = q3 + (1.5 * interquartile)
    # jadi batas bawah pengambilan range adalah -2
    print(" Batas outlier bawah : {}".format(batasOutlierBawah))
    # jadi batas atas pengambilan range adalah 6
    print(" Batas outlier atas : {}".format(batasOutlierAtas))
    if (batasOutlierBawah < arrayMDScoreSorted[0]):
        batasBawah = arrayMDScoreSorted[0]
    else:
        batasBawah = batasOutlierBawah
    if (batasOutlierAtas > arrayMDScoreSorted[len(arrayMDScoreSorted) - 1]):
        batasAtas = arrayMDScoreSorted[len(arrayMDScoreSorted) - 1]
    else:
        batasAtas = batasOutlierAtas
    print("RULE penggolongan ===============================================")
    maxRangeMD = batasAtas - batasBawah
    print(" RANGE : {}".format(maxRangeMD))
    level = ["NA", "low", "moderate", "high"]
    if request.method == 'POST':
        # formYangAkanDiSave = TweetForm(request.POST or None)
        formYangAkanDiSave = TweetForm(request.POST)

        if formYangAkanDiSave.is_valid():
            tweet = formYangAkanDiSave.save(commit=False)
            print("# raw text : {}".format(tweet.tweet))

           ####PREPROCESS
            text = getFivePreprocess(tweet.tweet)  # NUMBER, LINK, MENTION, # sign (removal), Dont (conversion)
            tokens = word_tokenize(text)
            token_2 = []
            lemma_2 = []
            for token, tag in pos_tag(tokens):  #PREPROCESS - lematisasi ubah menjadi kata dasar
                lemma = wn_lemmater.lemmatize(token, tag_map[tag[0]])
                token_2.append(token)
                lemma_2.append(lemma)
            text = ' '.join(lemma_2)

            word_tokens = cleanByRegex = preprocess(text, True)# PREPROCESS - #LOWERCASE ALL    # TOKEN NEEDED TYPE (word)

            filtered_sentence = [] #INI ADALAH CLEANED TWEET
            for w in word_tokens:
                # PREPROCESS - STOPWORD REMOVAL
                if w not in stop_words:
                    filtered_sentence.append(w)  # untuk tampilan per tweet
            #         # ALL_filtered_sentence.append(w)  # untuk mencari kata dari seluruh data training
            # tweet.cleanTweet = filtered_sentence
            print("# {} hasil preprocess tweet (cleanTweet)        : {} ".format(len(filtered_sentence), filtered_sentence))


            # 1st - SELF REFERENCES - check
            selfScore = False
            for word in filtered_sentence:
                if word in self_words:
                    selfScore = True
                    break
            tweet.selfScore = selfScore
            # print("# selfScore : {}".format(tweet.selfScore))

            #2nd - SENTIMENT SCORE
            tweet.negSentiScore, tweet.posSentiScore, tweet.neuSentiScore = tweet.countSentiScore(tweet.tweet)  # assign score of 3 value (-,+,0)
            tweet.uniSentimen, tweet.multiSentimen = tweet.countSentiment(tweet.tweet)  # assign score of 2 value of textual sentimen ('positive', )

            #3rd - MDSCORE
            #4th - NEG EMO
            MDscore_count = 0
            temNegScore = 0
            for word in filtered_sentence:
                if word in wordSenticList:
                    MDscore_count = MDscore_count + 1
                    # findWord = wordSenticRepo.searchWord(word)
                    findWord = WordList_sentic.objects.filter(word=word)
                    for find in findWord:
                        sens = find.senticnet.sentics.sensitivity
                        plea = find.senticnet.sentics.pleasantness
                        apti = find.senticnet.sentics.aptitude
                        temNegScore = temNegScore + (-1 * abs(float(sens)))
                        if (float(plea) < 0):
                            print("sentics pleasantness : {}".format(plea))
                            temNegScore = temNegScore + float(plea)
                        if (float(apti) < 0):
                            print("sentics aptitude : {}".format(apti))
                            temNegScore = temNegScore + float(apti)
            tweet.negEmo = temNegScore
            tweet.MDscore = MDscore_count
            print("#SKOR Self Reference              : {}".format(tweet.selfScore))
            print("#SKOR negative sentiscore         : {}".format(tweet.negSentiScore))
            print("#SKOR Negativity Emotion          : {}".format(tweet.negEmo ))
            print("#SKOR Mental Disorder             : {}".format(tweet.MDscore))

            temLabel = ""
            if tweet.selfScore != True:
                temLabel = level[0]
                print(temLabel)
            else:
                if tweet.negSentiScore > 0:  # kalimat bersentimen Negatif
                    if tweet.negEmo != 0:  # tingkat sadnessnya ADA
                        if tweet.MDscore < maxRangeMD / 3:
                            temLabel = level[2]  # MODERATE 1
                        else:
                            temLabel = level[3]  # MOST 2
                    else:  # tingkat sadnesnya tidak ada
                        print("kalimatnya tidak mengandung sadness")
                        if tweet.MDscore < maxRangeMD / 3:
                            temLabel = level[1]  # LEAST 3
                        elif tweet.MDscore < (maxRangeMD / 3 * 2):
                            temLabel = level[2]  # MODERATE 4
                        else:
                            temLabel = level[3]  # MOST 5
                else:  # kalimat bersentimen positif
                    print("kalimat nya positif")
                    if tweet.negEmo != 0:  # tingkat sadnessnya ADA
                        if tweet.MDscore < maxRangeMD / 3:
                            temLabel = level[1]  # LEAST 6
                        else:
                            temLabel = level[2]  # MODERATE 7
                    else:  # tingkat sadnesnya tidak ada
                        print("kalimatnya tidak mengandung sadness")
                        if tweet.MDscore < maxRangeMD / 3:
                            temLabel = level[1]  # LEAST 8
                        elif tweet.MDscore < (maxRangeMD / 3 * 2):
                            temLabel = level[1]  # LEAST 9
                        else:
                            temLabel = level[2]  # MODERATE 10

                tweet.label = temLabel

            # print("# MDccore : {}".format(tweet.MDscore))

            # senticsObj = Sentics()
            # senticsObj.pleasantness = 1
            # senticsObj.aptitude = 2
            # senticsObj.attention =22
            # senticsObj.sensitivity = 33
            #
            # senticnetObj = Senticnet()
            # senticnetObj.sentics = senticsObj
            # senticnetObj.polarity_intense = 22
            # senticnetObj.polarity_value = 'positive'

            # post.senticnet = senticnetObj

            tweet.save()
            # pk = post.objects.get('_id')
            # pk = post._id
            # print("pk pk tweet : {}".format(pk))
            # print("pk tweet : {}".format(post.pk))
            return redirect('result', pk=tweet.pk)
    else:
        form = TweetForm()
    return render(request, template_name, {'formInputTweet':form, 'wordlist':wordlist, 'traindata':traindata} )

def showResult(request, pk):

    tweetText = get_object_or_404(TweetText, pk=pk)
    return render(request, 'mdcek/result.html', {'tweetText': tweetText})