from django.db import models
from django.conf import settings
from django.utils import timezone

from djongo import models
from django import forms

import spacy
nlp = spacy.load('en_core_web_sm')




# Create your models here.

#BLOG terdiri atas name dan tagline
# class Blog(models.Model):
# #     name = models.CharField(max_length=100)
# #     tagline = models.TextField()
# #
# #     class Meta:
# #         abstract = True

#ENTRY terdiri atas blog dan headline
# class Entry(models.Model):
#     blog = models.EmbeddedModelField(
#         model_container=Blog,  #class in class
#     )
#     headline = models.CharField(max_length=255)

# https://tutorial.djangogirls.org/en/django_models/
class Post(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now())
    published_date = models.DateTimeField(blank=True, null=True)

    #void method
    def publish(self):
        self.published_date =timezone.now()
        self.save()

    #method that return something
    def __str__(self):
        return self.title

class Sentics(models.Model):
    pleasantness = models.FloatField()
    attention = models.FloatField()
    sensitivity = models.FloatField()
    aptitude = models.FloatField()
    class Meta:
        abstract = True
class SenticsForm(forms.ModelForm):
    class Meta:
        model = Sentics
        fields = ('pleasantness', 'attention', 'sensitivity', 'aptitude')
class Moodtags(models.Model):

    class Meta:
        abstract = True
class Semantics(models.Model):
    class Meta:
        abstract = True
class Senticnet(models.Model):
    # moodtags = models.ArrayModelField(
    #     model_container= Moodtags
    # )
    # semantics = models.ArrayModelField(
    #     model_container= Semantics
    # )
    moodtags = models.ListField()
    semantics = models.ListField()
    sentics = models.EmbeddedModelField(
        model_container=Sentics,
        # model_form_class=SenticsForm
    )
    polarity_value = models.CharField(max_length=200)
    polarity_intense = models.FloatField()

    class Meta:
        abstract = True

    # def __str__(self):
    #     return self.polarity_value

class SenticnetForm(forms.ModelForm):
    class Meta:
        model = Senticnet
        fields = ('sentics', 'polarity_value', 'polarity_intense')


class WordList_sentic(models.Model):
    _id = models.ObjectIdField()
    word = models.CharField(max_length=200)
    senticnet = models.EmbeddedModelField(
        model_container = Senticnet,
        model_form_class = SenticnetForm,
    )
    def __str__(self):
        return self.word

#MY GLOBAL VARIABEL
self_words = ['my', 'myself', 'i', "i'", 'self', 'am', 'me', 'id', "i'd", "'d", "ain", "ain't",
                  "i'll", 'im', "i'm", "ive","i've",
                  "mine", "own", 'myselves', 'ourselves', "'ve"]
class TrainData(models.Model):
    # objects = models.DjongoManager()
    _id = models.ObjectIdField()
    username = models.CharField(max_length=200)
    tweet = models.TextField()
    # cleanTweet = models.ListField()
    selfScore = models.BooleanField()
    negSentiScore = models.FloatField()
    negEmo = models.FloatField()
    MDscore = models.IntegerField()
    label = models.CharField(max_length=200)


    def getMDscore(self):
        return self.MDscore

    def __str__(self):
        return self.tweet

class TweetText(models.Model):
    # objects = models.DjongoManager()
    _id = models.ObjectIdField()
    tweet = models.TextField()
    # cleanTweet = models.ListField()
    selfScore = models.BooleanField()

    negSentiScore = models.FloatField()
    posSentiScore = models.FloatField()
    neuSentiScore = models.FloatField()

    negEmo = models.FloatField()
    MDscore = models.IntegerField()
    label = models.CharField(max_length=200)


    uniSentimen = models.CharField(max_length=200)
    multiSentimen = models.CharField(max_length=200)
    # senticnet = models.EmbeddedModelField(
    #     model_container=Senticnet,
    #     # model_form_class=SenticnetForm,
    # )
    def countSentiScore(self, text):
        doc = nlp(text)
        negSentiScore = doc._.sentimenter['neg']
        posSentiScore = doc._.sentimenter['pos']
        neuSentiScore = doc._.sentimenter['neu']
        return negSentiScore, posSentiScore, neuSentiScore

    def countSentiment(self, text):
        neuSentimen = self.neuSentiScore
        negSentimen = self.negSentiScore
        posSentimen = self.posSentiScore
        multiSentimen =''
        if (neuSentimen > (negSentimen + posSentimen)):  # jika skor netral lebih banyak dari kecederungan
            multiSentimen = multiSentimen + ' neutral'
            uniSentimen = 'neutral'
        elif negSentimen > posSentimen:  # jika skor kecenderungan lebih besar, dan cenderung negative
            multiSentimen = multiSentimen + ' negative'
            uniSentimen =  'negative'
        elif posSentimen > negSentimen:  # jika skor kecenderungan lebih besar, dan cenderung positif
            multiSentimen = multiSentimen + ' positive'
            uniSentimen = 'positive'
        else:  # else jika skor kecenderungan lebih besar, tapi skor kecenderungan + - sama
            uniSentimen = 'neutral'
        if negSentimen > posSentimen:  # jika skor kecenderungan lebih besar, dan cenderung negative
            multiSentimen = multiSentimen + ' negative'
        if posSentimen > negSentimen:  # jika skor kecenderungan lebih besar, dan cenderung positif
            multiSentimen = multiSentimen + ' positive'
        return uniSentimen, multiSentimen

    def __str__(self):
        return self.tweet

# class WorList_sentic(models.Model):
#     word = models.CharField(max_length=200)
#     sentic =

# from djongo import models
# from django import forms
#
# class BlogContent(models.Model):
#     comment = models.CharField(max_length=200)
#     author = models.CharField(max_length=200)
#     class Meta:
#         abstract = True
#
# # Form definition for the above model
# class BlogContentForm(forms.ModelForm):
#     class Meta:
#         model = BlogContent
#         fields = ('comment', 'author')
#
# # Now “embed” your BlogContent inside a BlogPost
# # using the EmbeddedModelField as below:
# class BlogPost(models.Model):
#     judul = models.CharField(max_length=200)
#     content = models.EmbeddedModelField(
#         model_container = BlogContent,
#         model_form = BlogContentForm
#     )


# TUTORIAL : https://nesdis.github.io/djongo/using-django-with-mongodb-data-fields/


class Blog(models.Model):
    name = models.CharField(max_length=200)
    tagline = models.TextField()

    #untuk menjaga parentesisi kelas
    class Meta:
        abstract = True
    # def __str__(self):
    #     return self.name
class BlogForm(forms.ModelForm):
    class Meta:
        model = Blog
        fields = ('name', 'tagline')

class MetaData(models.Model):
    pub_date = models.DateField()
    mod_date = models.DateField()
    n_pingbacks = models.IntegerField()
    rating = models.IntegerField()

    class Meta:
        abstract = True
class MetaDataForm(forms.ModelForm):
    class Meta:
        model = MetaData
        fields = ('pub_date', 'mod_date', 'n_pingbacks', 'rating')

class Author(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField()

    def __str__(self):
        return self.name

# one entry one blog
# one blog many entry
class Entry(models.Model):
    blog = models.EmbeddedModelField(
        model_container= Blog,
        model_form_class=BlogForm,
    )
    meta_data = models.EmbeddedModelField(
        model_container= MetaData,
        model_form_class=MetaDataForm,
    )

    headline = models.CharField(max_length=200)
    body_text = models.TextField()
    authors = models.ManyToManyField(Author)
    n_comments = models.IntegerField()
    def __str__(self):
        return self.headline

# class AuthorForm(forms.ModelForm):
#     class Meta:
#         model = Author
#         fields = ('name', 'email')
#
#
