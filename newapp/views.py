from django.shortcuts import render, get_object_or_404, redirect

# dot is the current direktori
from .models import Post
from .forms import PostForm
from .forms import TweetForm
from django.utils import timezone

# Create your views here.
def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now())
    # Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    #request, file template HTML, variable yang direturn
    return render(request, 'newapp/post_list.html', {'posts':posts})

# parameter bernama harus persis dengan yang di define di urls.py
def post_detail(request, pk):
    # Post.objects.get(pk=pk)
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'newapp/post_detail.html', {'post':post})
    # return 0;

# pertama, dia masuk ke post_edit untuk insert new data
# jika sudah di POST, akan dicek kemudian disave, next redirect ke post_detail
def post_new(request):
    if request.method == 'POST':
        formYangAkanDiSave = PostForm(request.POST)
        if formYangAkanDiSave.is_valid():

            post = formYangAkanDiSave.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'newapp/post_edit.html', {'form' : form})

# def input_tweet(request):
#     return render(request, 'newapp/input_tweet.html', {'tweet':tweet})

def indexTes(request):
    if request.method == 'POST':
        formYangAkanDiSave = TweetForm(request.POST)
        if formYangAkanDiSave.is_valid():
            tweet = formYangAkanDiSave.save(commit=False)
            # post.author = request.user
            # post.published_date = timezone.now()
            tweet.save()
            return redirect('indexTes')
    else:
        forma = TweetForm()
    return render(request, 'mdcek/index.html', {'formInputTweet':forma} )



