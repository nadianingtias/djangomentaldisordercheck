Merupakan implementasi web sederhana dari metode deteksi tingkat depresi pada teks sosial media platform Twitter.

Dibangun menggunakan web framework Django, menggunakan IDE PyCharm.
Data lexicon dan train dapat disesuaikan atau dapat diperoleh dengan menghubungi email :
nadia.ningtias20@gmail.com

Route halaman input teks : 
xxxxx:8000/home

GITLAB CLONE LINK :
https://gitlab.com/nadianingtias/djangomentaldisordercheck.git

