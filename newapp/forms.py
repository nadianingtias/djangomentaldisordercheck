from django import forms

from .models import Post
from .models import TweetText
from .models import TrainData

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'text', )

class TweetForm(forms.ModelForm):
    class Meta:
        model = TweetText
        fields = ('tweet',)


class TrainDataForm(forms.ModelForm):
    class Meta:
        model = TrainData
        fields = ('tweet','username','selfScore', 'negSentiScore', 'MDscore', 'negEmo', 'label')