"""django_cekMD URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.contrib import admin
from django.urls import path, include
from . import views
from . import views_app



urlpatterns = [

    #path (url , func views yang) menghandle,(urls name defined) nama url yang akan dipanggil di template  )
    path('', views.post_list, name='post_list'),
    # post/<int:pk>
    #  It means that Django expects an integer value and will transfer it to a view as a variable called pk

    path('post/<int:pk>/', views.post_detail , name='post_detail'),
    path('post/new/', views.post_new , name='post_new'),
    # path('cekmd/', views.input_tweet, name='input_new'),

    # python
    # cekMD
    path('tes/', views.indexTes, name='indexTes'),
    path('home/', views_app.index, name='home'),
    path('result\\/(?P<pk>[0-9]+)\\/$', views_app.showResult, name = 'result')

]
