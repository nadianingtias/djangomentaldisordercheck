from django.contrib import admin
from .models import Post
from .models import Author, Entry
from .models import WordList_sentic
# from .models import TrainData
# from .embedded_models import  Author, Entry
# from .models import BlogPost

# Register your models here.
# https://tutorial.djangogirls.org/en/django_admin/
# To add, edit and delete the posts we've just modeled, we will use Django adm
admin.site.register(Post)
# admin.site.register([Blog, Author, Entry])
admin.site.register([Author, Entry])
admin.site.register([WordList_sentic])
# admin.site.register(BlogPost)